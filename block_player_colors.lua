--[[
Copyright 2017 "Kovus" <kovus@soulless.wtf>

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

	block_player_colors.lua - FishBus' restrictions on player colors
	
	Once we put player colors into the player list, it become fairly obvious
	that we needed to restrict players from using colors that were impossible
	to read in the player list (and it turns out, on the map).
	Specifically, any colors with too low of an alpha value (0) to be readable.

--]]

require 'event'
require 'event_softmod_init'

Event.register(defines.events.on_console_command, function(event)
	if event.command == "color" then
		local player = game.players[event.player_index]
		local color = player.color
		if color.a < 0.5 then
			player.print({'bpc.alpha_low', 0.5})
			color.a = 0.5
			player.color = color
		end
		-- RGB Delta calculation.  If the delta is too small to the 
		-- perceived gray background (0.27, 0.27, 0.27) then block the color.
		local badgray = {r=0.27, g=0.27, b=0.27}
		local deltae = math.sqrt(
			math.pow(badgray.r - color.r, 2) +
			math.pow(badgray.g - color.g, 2) +
			math.pow(badgray.b - color.b, 2)
		)
		if deltae < 0.1 then
			player.print({'bpc.blocked_color', 'white'})
			player.color = {r = 1, g = 1, b = 1, a = 1}
		end
	end
end)
