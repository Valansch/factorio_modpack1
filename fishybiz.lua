--[[
Copyright 2017 "Kovus" <kovus@soulless.wtf>

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

	fishbiz.lua
	
	Spend fish to upgrade your player!

--]]

require 'event'
require 'event_softmod_init'

require 'kwidgets'

local upg_definition = require 'fishybiz_upgrades_definition'

fishbiz = {}

function fishbiz.apply_upgrade(player, upgrade, updata)
	local perk_name = upgrade.perk
	local perkValue = loadstring("return game.players[...]."..perk_name)(player.index)
	perkValue = perkValue + updata.increase
	loadstring("game.players[...]."..perk_name.."="..perkValue)(player.index)
end

function fishbiz.bank_redraw_balance(plidx, container)
	if container.balflow then
		if container.balflow.balance then
			local pldata = fishbiz.player_data(plidx)
			container.balflow.balance.caption = pldata.balance
		end
	end
end

function fishbiz.buy_upgrade(player_idx, perk_name)
	local player = game.players[player_idx]
	local pldata = fishbiz.player_data(player_idx)
	local updata = fishbiz.next_upgrade(player_idx, perk_name)
	local upgrade = global.fishbiz.def[perk_name]
	if updata.cost < 0 then
		player.print({'fishbiz.no_more_ranks', upgrade.desc})
		return
	end
	-- check if player has enough fish.
	local fishcount = fishbiz.remove_fish(player_idx, updata.cost, true)
	if fishcount < 0 then
		player.print({'fishbiz.not_enough', upgrade.desc, fishcount, updata.cost})
		return
	end
	-- buy & apply upgrade
	pldata[perk_name] = updata.upidx
	fishbiz.apply_upgrade(player, upgrade, updata)
	
	-- notify user
	player.print({'fishbiz.purchased', upgrade.desc})
end

function fishbiz.can_do_bank_transaction(plidx)
	-- check if the user is within X units of a fish market, so they can do
	-- bank transactions.  (Arbitrary restriction to add to 'realism' or
	-- annoyance of game.)
	local playerpos = game.players[plidx].position
	local dist = 10
	local range = {
		{playerpos.x - dist, playerpos.y - dist},
		{playerpos.x + dist, playerpos.y + dist}
	}
	if #game.surfaces[1].find_entities_filtered({area=range, type='market'}) > 0 then
		return true
	end
	return false
end

function fishbiz.deposit(plidx, value)
	local player = game.players[plidx]
	if not fishbiz.can_do_bank_transaction(plidx) then
		player.print({'fishbiz.bank.deposit_not_available'})
		return 
	end
	-- remove provided value (-1 if not enough)
	local removed = fishbiz.remove_fish(plidx, value, false)
	if removed < 0 then
		player.print({'fishbiz.bank.deposit_not_enough', fishcount, value})
		return
	end
	-- deposit to bank.
	local pldata = fishbiz.player_data(plidx)
	pldata.balance = pldata.balance + removed
	player.print({'fishbiz.bank.deposited', removed})
	return pldata.balance
end

function fishbiz.deposit_all(plidx)
	local player = game.players[plidx]
	local inv1 = player.get_inventory(defines.inventory.player_main)
	local inv2 = player.get_inventory(defines.inventory.player_quickbar)
	local fishcount = inv1.get_item_count('raw-fish') + inv2.get_item_count('raw-fish')
	return fishbiz.deposit(plidx, fishcount)
end

function fishbiz.event_deposit(event)
	local amount = tonumber(event.element.parent.dep_value.text)
	if amount then
		fishbiz.deposit(event.player_index, amount)
		fishbiz.bank_redraw_balance(event.player_index, event.element.parent.parent)
	else
		local player = game.players[event.player_index]
		player.print({'fishbiz.bank.invalid_amount'})
	end
end
function fishbiz.event_deposit_all(event)
	fishbiz.deposit_all(event.player_index, amount)
	fishbiz.bank_redraw_balance(event.player_index, event.element.parent.parent)
end
function fishbiz.event_withdraw(event)
	local amount = tonumber(event.element.parent.wd_value.text)
	if amount then
		fishbiz.withdraw(event.player_index, amount)
		fishbiz.bank_redraw_balance(event.player_index, event.element.parent.parent)
	else
		local player = game.players[event.player_index]
		player.print({'fishbiz.bank.invalid_amount'})
	end
end
function fishbiz.event_withdraw_all(event)
	fishbiz.withdraw_all(event.player_index, amount)
	fishbiz.bank_redraw_balance(event.player_index, event.element.parent.parent)
end

function fishbiz.next_upgrade(player_idx, perk_name)
	local pldata = fishbiz.player_data(player_idx)
	local upgrade = global.fishbiz.def[perk_name]
	local upidx = 1
	local increase = 0
	local cost = 0
	if pldata and pldata[perk_name] then
		upidx = pldata[perk_name] + 1
	end
	local rank_count = #upgrade.rank_costs
	if rank_count < upidx then
		if upgrade.infinite then
			increase = upgrade.rank_costs[rank_count].increase
			cost = upgrade.rank_costs[rank_count].cost
		else
			cost = -1
		end
	else
		increase = upgrade.rank_costs[upidx].increase
		cost = upgrade.rank_costs[upidx].cost
	end
	return {
		upidx = upidx,
		increase = increase,
		cost = cost,
	}
end

function fishbiz.player_data(plidx)
	if not global.fishbiz.players[plidx] then
		global.fishbiz.players[plidx] = {
			balance = 0,
		}
	end
	return global.fishbiz.players[plidx]
end

function fishbiz.remove_fish(plidx, value, include_bank)
	local player = game.players[plidx]
	local pldata = fishbiz.player_data(plidx)
	local inv1 = player.get_inventory(defines.inventory.player_main)
	local inv2 = player.get_inventory(defines.inventory.player_quickbar)
	local available = 0
	if include_bank then
		available = available + pldata.balance
	end
	available = available + inv1.get_item_count('raw-fish')
	available = available + inv2.get_item_count('raw-fish')
	if available < value then
		return -1
	end
	local needed = value
	-- get through our sources, and remove fish:
	if needed > 0 and inv2.get_item_count('raw-fish') > 0 then
		local toremove = math.min(needed, inv2.get_item_count('raw-fish'))
		inv2.remove({name='raw-fish', count=toremove})
		needed = needed - toremove
	end
	if needed > 0 and inv1.get_item_count('raw-fish') > 0 then
		local toremove = math.min(needed, inv1.get_item_count('raw-fish'))
		inv1.remove({name='raw-fish', count=toremove})
		needed = needed - toremove
	end
	if needed > 0 and include_bank and pldata.balance > 0 then
		local toremove = math.min(needed, pldata.balance)
		pldata.balance = pldata.balance - toremove
		needed = needed - toremove
	end
	if not needed == 0 then
		game.print("DEBUG: Unexpected condition: fishbiz.remove_fish - remaining needed != 0")
	end
	return value
end

function fishbiz.render_buy_button(table, rownumber, field, celldata, settings)
	local upgrade = celldata.upgrade
	local text = '$'
	local tooltip = {'fishbiz.button_buy_tooltip'}
	if celldata.updata.cost < 0 then
		text = '-'
		tooltip = {'fishbiz.no_more_ranks', upgrade.desc}
	end
	button = kw_newButton(table, 'upgrade.btn.'..upgrade.perk, text, tooltip, nil, nil)
	return button
end

function fishbiz.upgrades_draw_table(player, container, sortfield, sortdir)
	local headers = {
		{name='desc',  text={'fishbiz.header_upgrade'}, field='desc'},
		{name='points',text={'fishbiz.header_points'},  field='points'},
		{name='value', text={'fishbiz.header_value'},   field='value'},
		{name='cost',  text={'fishbiz.header_price'},   field='cost'},
		{name='buy',   text={'fishbiz.header_buy'},     field='buy'},
	}
	local data = {}
	if player.character and player.character.valid then
		for key, upg in pairs(global.fishbiz.def) do
			updata = fishbiz.next_upgrade(player.index, key)
			table.insert(data, {
				desc = {text=upg.desc},
				points = {text=loadstring("return game.players[...]."..upg.perk)(player.index)},
				value = {text=updata.increase},
				cost = {text=updata.cost},
				buy = {render=fishbiz.render_buy_button, upgrade=upg, updata=updata},
			})
		end
	end
	local settings = {
		widths = { desc=200, points=80, value=75, cost=50, buy=50 },
		name = "UpgradesTable",
		scrollpane = { width=500, height=255 },
	}
	kw_table_draw(container, headers, data, settings)
end

function fishbiz.withdraw(plidx, value)
	local player = game.players[plidx]
	if value <= 0 then
		player.print({'fishbiz.bank.withdraw_0'})
		return
	end
	if not fishbiz.can_do_bank_transaction(plidx) then
		player.print({'fishbiz.bank.withdraw_not_available'})
		return 
	end
	local pldata = fishbiz.player_data(plidx)
	if pldata.balance < value then
		player.print({'fishbiz.bank.withdraw_not_enough_fish', pldata.balance, value})
		return
	end
	local inv1 = player.get_inventory(defines.inventory.player_main)
	if inv1.can_insert({name='raw-fish', count=value}) then
		local actual_insert = inv1.insert({name='raw-fish', count=value})
		pldata.balance = pldata.balance - actual_insert
		player.print({'fishbiz.bank.withdraw', actual_insert})
		return pldata.balance
	end
end

function fishbiz.withdraw_all(plidx)
	local pldata = fishbiz.player_data(plidx)
	return fishbiz.withdraw(plidx, pldata.balance)
end


function fishbiz.init(event)
	global.fishbiz = {
		players = {},
		def = upg_definition,
	}
	
	kw_newTabDialog('fish_biz', 
		{caption={'fishbiz.window_title'}},
		{position='center', defaultTab='upgrades'}, 
		function(dialog)
			dialog:addTab('upgrades',
				{caption = {'fishbiz.upgrades.title'}, tooltip = {'fishbiz.upgrades.tooltip'}, },
				function(dialog, tab) -- instantiation.
					-- button connections, etc
					for idx, upg in pairs(global.fishbiz.def) do
						kw_connectButton('upgrade.btn.'..upg.perk, function(event)
							local perk = string.sub(event.element.name, 13)
							fishbiz.buy_upgrade(event.player_index, perk)
							local container = event.element.parent.parent.parent
							local player = game.players[event.player_index]
							fishbiz.upgrades_draw_table(player, container, 'desc', 'desc')
						end)
					end
				end,
				function(player, dialog, container) -- dialog render
					-- on display
					container.add({name='aboutmsg', type='label', caption={'fishbiz.aboutmsg'}})
					container.aboutmsg.style.single_line = false
					container.aboutmsg.style.maximal_width = container.style.maximal_width
			
					fishbiz.upgrades_draw_table(player, container, 'desc', 'desc')
				end
			) -- end upgrades
			dialog:addTab('bank',
				{caption = {'fishbiz.bank.title'}, tooltip = {'fishbiz.bank.tooltip'}, },
				function(dialog, tab) -- instantiation.
					-- button connections, etc
					kw_connectButton('fishbiz.bank.deposit', fishbiz.event_deposit)
					kw_connectButton('fishbiz.bank.deposit_all', fishbiz.event_deposit_all)
					kw_connectButton('fishbiz.bank.withdraw', fishbiz.event_withdraw)
					kw_connectButton('fishbiz.bank.withdraw_all', fishbiz.event_withdraw_all)
				end,
				function(player, dialog, container) -- dialog render
					-- on display
					container.add({name='aboutmsg', type='label', caption={'fishbiz.bank.about'}})
					container.aboutmsg.style.single_line = false
					container.aboutmsg.style.maximal_width = container.style.maximal_width
					
					-- balance
					local balflow = container.add({name='balflow', type='flow', direction='horizontal'})
					balflow.add({name='baltext', type='label', caption={'fishbiz.bank.balance_text'}})
					balflow.baltext.style.font = 'default-large'
					balflow.add({type='sprite', sprite='item/raw-fish'})
					local pldata = fishbiz.player_data(player.index)
					local balance = pldata.balance
					balflow.add({name='balance', type='label', caption={'fishbiz.bank.balance', balance} })
					balflow.balance.style.font = 'default-large'
					balflow.balance.style.font_color = { r=1, g=1, b=0, a=1 }
					kw_hline(container)
					-- deposit
					local depflow = container.add({type='flow', direction='horizontal'})
					depflow.add({name='dep_value', type='textfield', text='0'})
					kw_newButton(depflow, 'fishbiz.bank.deposit', {'fishbiz.bank.deposit_btn_text'}, {'fishbiz.bank.deposit_btn_tooltip'}, nil, button_style)
					kw_newButton(depflow, 'fishbiz.bank.deposit_all', {'fishbiz.bank.deposit_all_btn_text'}, {'fishbiz.bank.deposit_all_btn_tooltip'}, nil, button_style)
					
					kw_hline(container)
					-- withdraw
					local wdflow = container.add({type='flow', direction='horizontal'})
					wdflow.add({name='wd_value', type='textfield', text='0'})
					kw_newButton(wdflow, 'fishbiz.bank.withdraw', {'fishbiz.bank.withdraw_btn_text'}, {'fishbiz.bank.withdraw_btn_tooltip'}, nil, button_style)
					kw_newButton(wdflow, 'fishbiz.bank.withdraw_all', {'fishbiz.bank.withdraw_all_btn_text'}, {'fishbiz.bank.withdraw_all_btn_tooltip'}, nil, button_style)
				end
			) -- end upgrades
		end
	)
end

local function fishbiz_toggle(event)
	local player = game.players[event.player_index]
	local dialog = kw_getWidget('fish_biz')
	dialog:toggleShow(player)
end

Event.register("custom.softmod_init", fishbiz.init)

Event.register(defines.events.on_player_joined_game, function(event)
	local player = game.players[event.player_index]
	local dialog = kw_getWidget('fish_biz')
	kw_newToolbarButton(player, 'fish_upgrades_toggle', {'fishbiz.button_title'}, {'fishbiz.button_tooltip'}, 'item/raw-fish', fishbiz_toggle)
end)

Event.register(defines.events.on_player_respawned, function(event)
	-- need to reapply the player's upgrades!
	local pldata = fishbiz.player_data(event.player_index)
	local player = game.players[event.player_index]
	for key, upg in pairs(global.fishbiz.def) do
		if pldata[key] and pldata[key] > 0 then
			for idx = 1, pldata[key] do
				local updata = {}
				if idx > #upg.rank_costs then
					updata = upg.rank_costs[#upg.rank_costs]
				else
					updata = upg.rank_costs[idx]
				end
				fishbiz.apply_upgrade(player, upg, updata)
			end
		end
	end
end)
