--[[
	fish_ugprades_definition.lua
--]]

return {
	character_crafting_speed_modifier = {
		perk = 'character_crafting_speed_modifier',
		desc = {'fishbiz.perk.character_crafting_speed_modifier'},
		rank_costs = {
			{ increase = 2, cost = 1000, },
			{ increase = 2, cost = 2500, },
			{ increase = 3, cost = 5000, },
		},
		infinite = false, -- uses last rank above for repeat.
	},
	character_mining_speed_modifier = {
		perk = 'character_mining_speed_modifier',
		desc = {'fishbiz.perk.character_mining_speed_modifier'},
		rank_costs = {
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1500, },
			{ increase = 1, cost = 2000, },
		},
		infinite = false, -- uses last rank above for repeat.
	},
	character_running_speed_modifier = {
		perk = 'character_running_speed_modifier',
		desc = {'fishbiz.perk.character_running_speed_modifier'},
		rank_costs = {
			{ increase = 0.3, cost = 1000, },
			{ increase = 0.2, cost = 2000, },
			{ increase = 0.3, cost = 5000, },
		},
		infinite = false,
	},
	character_build_distance_bonus = {
		perk = 'character_build_distance_bonus',
		desc = {'fishbiz.perk.character_build_distance_bonus'},
		rank_costs = {
			{ increase = 2, cost = 500, },
			{ increase = 2, cost = 750, },
			{ increase = 2, cost = 1000, },
			{ increase = 2, cost = 1250, },
			{ increase = 2, cost = 1500, },
			{ increase = 2, cost = 1750, },
			{ increase = 2, cost = 2000, },
			{ increase = 2, cost = 2250, },
			{ increase = 2, cost = 2500, },
		},
		infinite = false,
	},
	--[[
	character_item_drop_distance_bonus = {
		perk = 'character_item_drop_distance_bonus',
		desc = {'fishbiz.perk.character_item_drop_distance_bonus'},
		rank_costs = {
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
		},
		infinite = false,
	},
	--]]
	character_reach_distance_bonus = {
		perk = 'character_reach_distance_bonus',
		desc = {'fishbiz.perk.character_reach_distance_bonus'},
		rank_costs = {
			{ increase = 2, cost = 500, },
			{ increase = 2, cost = 750, },
			{ increase = 2, cost = 1000, },
			{ increase = 2, cost = 1250, },
			{ increase = 2, cost = 1500, },
			{ increase = 2, cost = 1750, },
			{ increase = 2, cost = 2000, },
			{ increase = 2, cost = 2250, },
			{ increase = 2, cost = 2500, },
		},
		infinite = false,
	},
	--[[
	character_resource_reach_distance_bonus = {
		perk = 'character_resource_reach_distance_bonus',
		desc = {'fishbiz.perk.character_resource_reach_distance_bonus'},
		rank_costs = {
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
		},
		infinite = false,
	},
	character_item_pickup_distance_bonus = {
		perk = 'character_item_pickup_distance_bonus',
		desc = {'fishbiz.perk.character_item_pickup_distance_bonus'},
		rank_costs = {
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
		},
		infinite = false,
	},
	character_loot_pickup_distance_bonus = {
		perk = 'character_loot_pickup_distance_bonus',
		desc = {'fishbiz.perk.character_loot_pickup_distance_bonus'},
		rank_costs = {
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
			{ increase = 1, cost = 1000, },
		},
		infinite = false,
	},
	--]]
	quickbar_count_bonus = {
		perk = 'quickbar_count_bonus',
		desc = {'fishbiz.perk.quickbar_count_bonus'},
		rank_costs = {
			-- 1 quickbar is 1 quickbar row, not 1 slot!
			{ increase = 1, cost = 5000, },
			{ increase = 1, cost = 7500, },
		},
		infinite = false,
	},
	character_inventory_slots_bonus = {
		perk = 'character_inventory_slots_bonus',
		desc = {'fishbiz.perk.character_inventory_slots_bonus'},
		rank_costs = {
			{ increase = 10, cost = 5000, },
			{ increase = 10, cost = 5000, },
		},
		infinite = false,
	},
	character_logistic_slot_count_bonus = {
		perk = 'character_logistic_slot_count_bonus',
		desc = {'fishbiz.perk.character_logistic_slot_count_bonus'},
		rank_costs = {
			{ increase = 6, cost = 500, },
			{ increase = 6, cost = 1000, },
		},
		infinite = false,
	},
	character_trash_slot_count_bonus = {
		perk = 'character_trash_slot_count_bonus',
		desc = {'fishbiz.perk.character_trash_slot_count_bonus'},
		rank_costs = {
			{ increase = 6, cost = 750, },
			{ increase = 6, cost = 1500, },
		},
		infinite = false,
	},
	character_maximum_following_robot_count_bonus = {
		perk = 'character_maximum_following_robot_count_bonus',
		desc = {'fishbiz.perk.character_maximum_following_robot_count_bonus'},
		rank_costs = {
			{ increase = 25, cost = 500, },
		},
		infinite = false,
	},
	character_health_bonus = {
		perk = 'character_health_bonus',
		desc = {'fishbiz.perk.character_health_bonus'},
		rank_costs = {
			{ increase = 25, cost = 250, },
			{ increase = 25, cost = 500, },
			{ increase = 25, cost = 750, },
			{ increase = 25, cost = 1000, },
			{ increase = 25, cost = 1250, },
			{ increase = 25, cost = 1500, },
		},
		infinite = false,
	},
}
