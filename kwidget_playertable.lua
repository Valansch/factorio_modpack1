--[[
Copyright 2017 "Kovus" <kovus@soulless.wtf>

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

	kwidget_playertable.lua - Kovus' GUI Widget - player table.
	
	Inspired by ExplosiveGaming's player table.
	
	Builds a table of players, with an optional function for rendering
	additional commands or buttons, per-player.
	
--]]

require 'fb_util'
require 'kwidgets'
require 'kwidget_table_with_header'

local band_roles = require "band_roles"

function kw_playerTableSortItems()
	-- localizations in kwidgets.cfg
	return {
		{'kw.pTable.sort_id'},
		{'kw.pTable.sort_name'},
		{'kw.pTable.sort_time'},
		{'kw.pTable.sort_group'},
	}
end

-- these need to match up.  we can go from index to name
-- but 'short name' to index is harder.  I want descriptive code.
function kw_playerFilterList()
	return {
		{'kw.pTable.filter_both'},
		{'kw.pTable.filter_online'},
		{'kw.pTable.filter_offline'},
	}
end
function kw_playerFilterByname(str)
	strmap = {
		['both'] = 1,
		['online'] = 2,
		['offline'] = 3,
	}
	return strmap[str]
end

function kw_playerTable_columns(settings)
	local columns = {}
	if not settings.no_icons then
		table.insert(columns, {text={'playertable.header_icon'}, name='icon', sortable=false})
	end
	table.insert(columns, {text={'playertable.header_name', #game.connected_players}, name='playername', sortable=true})
	if settings.status then
		table.insert(columns, {text={'playertable.header_status'}, name='status', sortable=true})
	end
	table.insert(columns, {text={"playertable.header_played"}, name='time', sortable=true})
	if settings.distances and settings.distances.walked then
		table.insert(columns, {text={"playertable.header_walked"}, name='walked', sortable=true})
	end
	if settings.distances and settings.distances.driven then
		table.insert(columns, {text={"playertable.header_driven"}, name='driven', sortable=true})
	end
	if settings.distances and settings.distances.trained then
		table.insert(columns, {text={"playertable.header_trained"}, name='trained', sortable=true})
	end
	if settings.distances and settings.distances.total then
		table.insert(columns, {text={"playertable.header_travelled"}, name='travelled', sortable=true})
	end
	table.insert(columns, {text={"playertable.header_group"}, name='group', sortable=true})
	if settings.renderFunc then
		table.insert(columns, {
			text=settings.action_header or {'playertable.header_actions'}, 
			name='actions', 
			sortable=false,
		})
	end
	return columns
end

function kw_playerTable_icon_draw(table, rownumber, field, celldata, settings)
	local cell = table.add({
		type = "sprite",
		name = celldata.name or (rownumber .. field),
		sprite = celldata.sprite,
	})
	return cell
end

function kw_playerTable_player_icon(player)
	local player_role = player.tag:sub(2,-2)
	local role_icons = band_roles.roles[player_role]
	if role_icons then
		local icon = role_icons[math.random(1,#role_icons)]
		return {render=kw_playerTable_icon_draw, sprite=icon}
	end
	return {render=kw_playerTable_icon_draw, sprite='entity/player'}
end

function kw_playerTable_distances(player)
	local travel = {walked=0, trained=0, driven=0, total=0}
	local idx = player.index
	if remote.interfaces['pdistance'] then
		travel.driven = remote.call('pdistance', 'driven', idx)
		travel.driven_text = {
			'playertable.distance', 
			string.format("%0.3f", travel.driven / 1000)
		}
		travel.trained = remote.call('pdistance', 'trained', idx)
		travel.trained_text = {
			'playertable.distance', 
			string.format("%0.3f", travel.trained / 1000)
		}
		travel.walked = remote.call('pdistance', 'walked', idx)
		travel.walked_text = {
			'playertable.distance', 
			string.format("%0.3f", travel.walked / 1000)
		}
		travel.travelled = remote.call('pdistance', 'travelled', idx)
		travel.travelled_text = {
			'playertable.distance', 
			string.format("%0.3f", travel.travelled / 1000)
		}
	end
	return travel
end

function kw_playerTable_player_onlineTime(player)
	local time = tick2time(player.online_time)
	return {
		'playertable.online_time', 
		time.hours, 
		string.format("%02d", time.minutes), 
		string.format("%02d", time.seconds)
	}
end

function kw_playerTable_playerlist(settings)
	local sourcelist = {}
	local playerlist = {}
	if settings.playernamelist then
		for idx, name in pairs(settings.playernamelist) do
			local player = getPlayerNamed(name)
			if player then
				--if settings.connected_players and player.connected then
					table.insert(sourcelist, player)
				--elseif not settings.connected_players then
				--	table.insert(playerlist, player)
				--end
			end
		end
	elseif settings.connected_players then
		sourcelist = game.connected_players
	else
		sourcelist = game.players
	end

	for idx, player in pairs(sourcelist) do
		if not settings.filterFunc or (settings.filterFunc and settings.filterFunc(player, settings)) then
			local playerdata = {}
			playerdata.icon = kw_playerTable_player_icon(player)
			playerdata.playername = {
				text=player.name, 
				font="default-bold"
			}
			if settings.use_player_colors then
				playerdata.playername.color = player.color
			end
			playerdata.status = {text={'playertable.online'}, sortvalue=1}
			if not player.connected then
				playerdata.status = {text={'playertable.offline'}, sortvalue=2}
			end
			local dist = kw_playerTable_distances(player)
			playerdata.driven    = {text=dist.driven_text, sortvalue=dist.driven}
			playerdata.walked    = {text=dist.walked_text, sortvalue=dist.walked}
			playerdata.trained   = {text=dist.trained_text, sortvalue=dist.trained}
			playerdata.travelled = {text=dist.travelled_text, sortvalue=dist.travelled}
			playerdata.time = {text=kw_playerTable_player_onlineTime(player), sortvalue=player.online_time}
			local group = perms.playerGroup(player.name)
			playerdata.group = {text=group.i18n_sname, sortvalue=group.name}
			playerdata.actions = {render=settings.renderFunc, target=player}
			table.insert(playerlist, playerdata)
		end
	end
	return playerlist
end

function kw_playerTable_settings(settings)
	if not settings.widths then
		settings.widths = {}
	end
	settings.widths.icon = settings.widths.icon or 30
	settings.widths.playername = settings.widths.playername or 150
	settings.widths.status = settings.widths.status or 65
	settings.widths.time = settings.widths.time or 55
	settings.widths.group = settings.widths.group or 60
	settings.widths.actions = settings.widths.actions or 200
	
	settings.widths.walked    = settings.widths.walked    or 75
	settings.widths.driven    = settings.widths.driven    or 75
	settings.widths.trained   = settings.widths.trained   or 75
	settings.widths.travelled = settings.widths.travelled or 75
	
	settings.name = "PlayerTable"
	settings.scrollpane = settings.scrollpane or { width=545, height=265 }
	--settings.sort_field = settings.sort_field or 'playername'
	--settings.sort_dir = sortdir or "desc"
	--settings.on_header_click = test_headerClick
	return settings
end

function kw_playerTable_headerClick(event, container, header_field)
	local element = event.element
	local settings = global.playertable_settings[container.player_index .. "." .. container.name]

	local dir = "desc"
	if kw_table_header_is_desc(element.caption) then
		dir = "asc"
	end
	
	local headers = kw_playerTable_columns(settings)

	local header = headers[1].name
	for idx, entry in pairs(headers) do
		if kw_table_header_name_for(container, entry, settings) == element.name then
			-- selected header
			header = entry.name
		end
	end
	kw_playerTable_draw(container, header, dir)
end

function kw_playerTable(container, in_name, in_playerlist, in_settings, filterFunc, renderFunc)
	local settings = kw_playerTable_settings(in_settings or {})
	settings.table_name = in_name or 'playerTable'
	settings.name = in_name or 'playerTable'
	settings.playernamelist = in_playerlist
	settings.filterFunc = filterFunc
	settings.renderFunc = renderFunc
	settings.on_header_click = kw_playerTable_headerClick
	if not global.playertable_settings then
		global.playertable_settings = {}
	end
	player = game.players[container.player_index]
	global.playertable_settings[container.player_index .. "." .. container.name] = settings
	kw_playerTable_draw(container, 'playername', 'desc')
end

function kw_playerTable_draw(container, sort_field, sort_dir)
	local settings = global.playertable_settings[container.player_index .. "." .. container.name]

	local name = settings.table_name or 'playerTable'
	settings.sort_field = sort_field
	settings.sort_dir = sort_dir or "desc"
	
	-- only allow a single player table in the container
	if container[name] then
		container[name].destroy()
	end
	
	
	local columns = kw_playerTable_columns(settings)
	local data = kw_playerTable_playerlist(settings)
	kw_table_draw(container, columns, data, settings)

	return
end
